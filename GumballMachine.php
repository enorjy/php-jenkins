<?php


class GumballMachine{
    private $gumballs;

    //get amount of gumballs
    public function getGumballs(){
        return $this->gumballs;
    }
    //set amount of gumballs in the machine
    public function setGumballs($amount){
        $this->gumballs = $amount;
    }
    //user turns the wheel
    public function turnWheel(){
        $this->setGumballs($this->getGumballs() - 1);
    }
}
