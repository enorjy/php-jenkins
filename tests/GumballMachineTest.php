<?php


require 'GumballMachine.php';

class GumballMachineTest extends PHPUnit\Framework\TestCase
{
    public $gumballMachineInstance;

    public function setUp(){
        $this->gumballMachineInstance = new GumballMachine();
    }

    public function testIfWheelWorks(){
        $this->gumballMachineInstance->setGumballs(100);

        $this->gumballMachineInstance->turnWheel();

        $this->assertEquals(99, $this->gumballMachineInstance->getGumballs());
    }

    public function testIfWheelWorks2(){
        $this->gumballMachineInstance->setGumballs(20);

        $this->gumballMachineInstance->turnWheel();

        $this->assertEquals(19, $this->gumballMachineInstance->getGumballs());
    }

    public function testIfWheelWorks3(){
        $this->gumballMachineInstance->setGumballs(10);

        $this->gumballMachineInstance->turnWheel();

        $this->assertEquals(9, $this->gumballMachineInstance->getGumballs());
    }

}

